section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_copy
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
 
%define stdin 0
%define stdout 1
%define sys_read 0
%define sys_write 1
%define sys_exit 60
%define def_minus 0x2D
%define def_space 0x20
%define def_tab 0x9
%define def_newline 0xA

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, sys_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    	.loop:
    		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:	
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    	call string_length
	pop rsi
	mov rdx, rax
	mov rax, sys_write
       	mov rdi, stdout
	syscall
	xor rax,rax
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, def_newline
; Принимает код символа и выводит его в stdout
print_char:
	push rdi
    	mov rsi, rsp
	mov rax, sys_write
    	mov rdi, stdout
	mov rdx, 1
	syscall
	pop rdi
	xor rax, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    	cmp rdi, 0 ;проверка на отрицательное
    	jge print_uint
    	push rdi
	mov rdi, def_minus
	call print_char
	pop rdi
	neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push r9 	;чисто из соглашений
	push rbx 	;same
	mov rax, rdi
	mov rbx, 10
	mov r9, rsp	;сохраняем где-нибудь указатель на стек
	push 0
	.loop:
		xor rdx, rdx
		div rbx 		;в rdx остаток, в rax частное
		add rdx, '0'	 	; rdx = rdx + '0'
		dec rsp
		mov byte[rsp], dl 	;грустно что push не умеет в байты
		test rax, rax
		jnz .loop

	mov rdi, rsp
	push r9		;надо сделать на всякий случай, чисто из соглашений
	call print_string
	pop r9
	mov rsp, r9

	pop rbx		;возвращаем на круги своя
	pop r9
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx,rcx

    .loop:
    	mov al, byte[rsi+rcx]
    	cmp [rdi+rcx], al
	jne .not_equal
	inc rcx
	cmp al,0
	jne .loop
	
	mov rax,1
	ret
	.not_equal:
		xor rax,rax
    		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    	mov rax, sys_read
	push rax
    	mov rdi, stdin
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret 
;вспомогательная функиця, возвращающая 0, если в rdi пробел, табуляция или перевод строки,иначе возвращает rdi
is_visible_char:
	cmp rdi, def_space
	je .tru
	cmp rdi, def_tab
	je .tru
	cmp rdi, def_newline
	je .tru
	mov rax, rdi
	ret
	.tru:
		xor rax,rax
		ret



; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r9	
	push r10
	push rbx

	mov rbx, rsi	;размер буфера
	dec rbx		;оставляем место для 0
	mov r9, rdi 	;start of buffer
	mov r10, 1	;счётчик
	.spaces:
		call read_char
		cmp rax, 0
		je .error
		mov rdi, rax
		call is_visible_char
		cmp rax, 0
		je .spaces
	mov [r9], al
	.loop:	
		call read_char
		mov rdi, rax
		call is_visible_char
		cmp rax, 0
		je .end
		cmp rcx, rbx
		je .error
		mov [r9 + r10], al
		inc r10
		jmp .loop
	.end:
		xor rax,rax
		mov [r9 + r10], al
		mov rax, r9
		mov rdx, r10
		jmp .exit
	.error:
		xor rax, rax
		xor rdx, rdx
	.exit:
		pop rbx
		pop r10
		pop r9
		ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push rbx	;по моему сохранять регистры это хороший тон 
    	push r9
	xor rbx, rbx
	xor rax, rax
    	xor rdx, rdx
	.loop:
    		mov bl,byte[rdi+rdx]
		cmp rbx, 0
		je .end
		sub rbx, '0'
		jb .end
		cmp rbx, 9
		ja .end
		mov r9, rax	;mul 10
		sal rax, 2	;
		add rax, r9	;
		sal rax, 1	;mul 10
		add rax, rbx
		inc rdx
		jmp .loop
	.end:
		pop r9
		pop rbx
    		ret
	



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], def_minus
    	jne parse_uint ;достаточно прыгнуть в беззнаковое
	
	push rbx	;сохраняемся
	inc rdi
	call parse_uint
	dec rdi	;я просто вернул всё как было
	cmp rdx, 0
	je .exit
	inc rdx
	neg rax
	.exit:	
		pop rbx
    		ret 

; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rdx, rax
	jb .buff_overflow
	.loop:
		mov r9b, [rdi]
		mov [rsi], r9b
		inc rdi
		inc rsi
		test r9b, r9b
       		jnz .loop		
	ret
	.buff_overflow:
		xor rax, rax
		ret

